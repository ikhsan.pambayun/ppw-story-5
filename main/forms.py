from django import forms
from . import models

class MatkulForm(forms.ModelForm):
    class Meta:
        model = models.Matkul
        fields = '__all__'