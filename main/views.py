from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import *
from .forms import MatkulForm

def home(request):
    return render(request, 'main/home.html')

def tambah(request):
    form = MatkulForm()

    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form':form}
    
    return render(request, 'main/tambah.html', context)

def list(request):
    data = Matkul.objects.all()

    if request.method == "POST" :
        if request.POST['answ'] != '':
            get_object_or_404(Matkul, id=int('answ')).delete()
            return redirect("/list") 
        else:
            return redirect("/list")


    context = {'list':data}

    return render(request, 'main/list.html', context)

def detail(request, id):
    data = get_object_or_404(Matkul, id=id)

    context = {'matkul':data}

    return render(request, 'main/detail.html', context)

def hapus(request, id):
    data = get_object_or_404(Matkul, id=id)

    context = {'matkul':data}

    if request.method == "POST" :
        if request.POST['answ'] == 'Yes':
            data.delete()
            return redirect("/list") 
        else:
            return redirect("/list")


    return render(request, 'main/hapus.html', context)
