from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah/', views.tambah, name='tambah'),
    path('list/', views.list, name='list'),
    path('list/detail/<int:id>', views.detail, name='detail'),
    path('list/hapus/<int:id>', views.hapus, name='hapus'), 
]
