from django.db import models

class Matkul(models.Model):
    nama = models.CharField(max_length = 50,)
    dosen = models.CharField(max_length = 100)
    sks = models.PositiveSmallIntegerField()
    desk = models.CharField(max_length = 200)
    sem = models.CharField(max_length = 20)
    thn = models.CharField(max_length = 50)
    ruang = models.CharField(max_length = 50)

    def __str__ (self):
        return self.nama